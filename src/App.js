import React, { Component } from 'react';
import Nav from './Components/Layout/Navbar';
import Footer from './Components/Layout/Footer';
import Comandera from './Components/Layout/Comandera';

class App extends Component {
  render() {
    return (
      <div className="App">
      <Nav/>
      <Comandera/>
      <Footer/>
      </div>
    );
  }
}

export default App;
