const foods = [
    {
      id:1,  
      name: "Pizza",
      price: 400,
      image: "https://i.imgur.com/eTmWoAN.png",
      quantity: 0
    },
    {
        id:2 ,
      name: "Salad",
      price: 150,
      image: "https://i.imgur.com/DupGBz5.jpg",
      quantity: 0
    },
    {
        id: 3,
      name: "Sweet Potato",
      price: 120,
      image: "https://i.imgur.com/hGraGyR.jpg",
      quantity: 0
    },
    {
        id: 4,
      name: "Gnocchi",
      price: 500,
      image: "https://i.imgur.com/93ekwW0.jpg",
      quantity: 0
    },
    {
        id: 5,
      name: "Pot Roast",
      price: 350,
      image: "https://i.imgur.com/WCzJDWz.jpg",
      quantity: 0
    },
    {
        id: 6,
      name: "Lasagna",
      price: 750,
      image: "https://i.imgur.com/ClxOafl.jpg",
      quantity: 0
    },
    {
        id: 7,
      name: "Hamburger",
      price: 400,
      image: "https://i.imgur.com/LoG39wK.jpg",
      quantity: 0
    },
    {
        id: 8,
      name: "Pad Thai",
      price: 475,
      image: "https://i.imgur.com/5ktcSzF.jpg",
      quantity: 0
    },
    {
        id: 9,
      name: "Almonds",
      price: 75,
      image: "https://i.imgur.com/JRp4Ksx.jpg",
      quantity: 0
    },
    {
        id: 10,
      name: "Bacon",
      price: 175,
      image: "https://i.imgur.com/7GlqDsG.jpg",
      quantity: 0
    },
    {
        id: 11,
      name: "Hot Dog",
      price: 275,
      image: "https://i.imgur.com/QqVHdRu.jpg",
      quantity: 0
    },
    {
        id: 12,
      name: "Chocolate Cake",
      price: 490,
      image: "https://i.imgur.com/yrgzA9x.jpg",
      quantity: 0
    },
    {
        id:13,
      name: "Wheat Bread",
      price: 175,
      image: "https://i.imgur.com/TsWzMfM.jpg",
      quantity: 0
    },
    {
        id: 14,
      name: "Orange",
      price: 85,
      image: "https://i.imgur.com/abKGOcv.jpg",
      quantity: 0
    },
    {
        id: 15,
      name: "Banana",
      price: 175,
      image: "https://i.imgur.com/BMdJhu5.jpg",
      quantity: 0
    },
    {
        id: 16,
      name: "Yogurt",
      price: 125,
      image: "https://i.imgur.com/URhdrAm.png",
      quantity: 0
    }
  ]
  
  export default foods;