import React, { Component } from "react";
import { Navbar,NavItem, Col, Row,Icon, SideNav, SideNavItem } from 'react-materialize';
import background from '../../assets/inside-the-restaurant.jpg'
import user from '../../assets/ioelegenate.jpg'


class Nav extends Component {
  render() {
    return (
      <div>
      <Row>
      <Col l={8}>
      <Navbar  className="navbar">  
        <NavItem>
        <SideNav
  trigger={ <i className="material-icons">all_inclusive</i>}
 // trigger={<NavItem><Icon>power_settings_new</Icon></NavItem>}
  options={{ closeOnClick: true }}
  >
  <SideNavItem userView
    user={{
      background:background,
      image: user,
      name: 'Emmanuel Juarez',
      email: 's.e.jugr@gmail.com'
    }}
  />
  <SideNavItem waves icon='face'>Perfil de usuario</SideNavItem>
  <SideNavItem waves icon='directions_run'>Logout/Salir</SideNavItem>
  <SideNavItem divider />
  <SideNavItem subheader>Menu de Trabajo</SideNavItem>
  <SideNavItem waves icon='restaurant'>Crea nueva mesa</SideNavItem>
  <SideNavItem waves icon='exposure_plus_1'>Crea nueva comanda</SideNavItem>
  <SideNavItem subheader>Menu de Comandas</SideNavItem>
  <SideNavItem waves icon='done_all'>Ver todas las comandas</SideNavItem>
</SideNav>
        </NavItem> 
         
       <NavItem right><Icon>search</Icon></NavItem>
       <NavItem><Icon>view_module</Icon></NavItem>

      </Navbar>
      </Col>

      <Col l={4}>
        <Navbar className="navbar2" right>
            <ul>
                <li>Atiende: Emmanuel</li>
                <li><i className=' large material-icons'>local_dining</i></li>
                <li><i className=" large material-icons">person_pin</i></li>
            </ul>
        </Navbar>
      </Col>
      </Row>
      </div>
    );
  }
}
export default Nav;
