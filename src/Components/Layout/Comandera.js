import React, { Component } from 'react';
import { Row,Input,Icon, Col,Button } from 'react-materialize';
import foods from '../../foods';
import FoodBox from './FoodBox';


class Comandera extends Component {
  constructor(props){
    super(props)
    this.state={
      search: "",
      todayFoods: [],
      qty:1,
      tickets:[]
    }
  }
  
  handleClick = (food, qty) => {
    let todayFoods = this.state.todayFoods.slice();
    todayFoods.push({...food, qty});
    this.setState({
      todayFoods:todayFoods
    })
}
  displayFood = (search) =>{
    search = search.toUpperCase();
    let result = [];
    for (let i=0; i<foods.length; i++){
      if(foods[i].name.toUpperCase().indexOf(search) !== -1){
        result.push(
          <FoodBox food={foods[i]} key={i} onClick={this.handleClick}/>
        )
      }
    }
    return result;
  }

  selectedFoods = () => {
    let result = [];
    for (let i = 0; i < this.state.todayFoods.length; i++){
      result.push(
        <Row key={i}>
          <Col m={8}>
          <ul>
           <li key={i}>
           {this.state.todayFoods[i].qty}{this.state.todayFoods[i].name} ===
            {this.state.todayFoods[i].qty * this.state.todayFoods[i].price}
         </li>
        </ul>
          </Col>
          <Col m={4}>
          <Button
          key={i} 
          onClick={e => this.removeFood(i)}
          floating
          className='red'
          icon='remove' 
          waves='light'/>
          </Col>
        </Row>  
      )
    }
    //localStorage.setItem('products',result)
    return result
  }

  removeFood = (index) => {
     let todayFoods = this.state.todayFoods
    this.setState({
      todayFoods:todayFoods.filter((f,i) => i !== index)
    })
  }

   totalPrice = () => {
    let result = 0;
    for (let i = 0; i< this.state.todayFoods.length; i++){
      result += this.state.todayFoods[i].qty * this.state.todayFoods[i].price;
    }
    return result
  }

  saveTicket = () => {
   let total = this.totalPrice();
   let products = this.state.todayFoods;
   let tickets = this.state.tickets
   console.log('salvaste')
    tickets.push({products,total})
   this.setState({
     todayFoods:[]
   })
  }
  showTickets = () => {
    console.log(this.state.tickets)
    let tickets = this.state.tickets
    
    for (const ticket of tickets){
      console.log(ticket.products)
      console.log(ticket.total)
     let result=  Object.keys(ticket.products).map(prod => {
       return [ ticket.products[prod]]
     })//por resolver mostrar comandas
    }
  
  }
  render() {
   
    return (
      <div>
        <Row>
          <Input onChange={(e) => {this.setState({search: e.target.value})}} placeholder=" Busca Platillos" s={6}><Icon right>search</Icon></Input>
        </Row>

        <Row>
          <Col l={8}>
          <h4>Comidas</h4>
           { this.state.search?(this.displayFood(this.state.search)):(foods.map(food => {return  <FoodBox food={food} key={food.id} onClick={this.handleClick}/>}))}
          </Col>
          <Col l={4}>
            <h4>Comanda</h4>
            {this.selectedFoods()}
             Total: {this.totalPrice()} $$$
            <Row>
              
             <Col m={8}>
               Guarda Comanda
             </Col>
             <Col m={4}>
             <Button
                
                floating  
                waves='light' 
                icon='add' 
                className='red'
                onClick={this.saveTicket}  
                />
             </Col> 
            
            </Row>
            <hr></hr>
            {this.showTickets()}
          </Col>
        </Row>
      </div>
    )
  }
}

export default  Comandera