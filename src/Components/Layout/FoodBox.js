import React, {Component} from 'react'
import { Card, CardTitle, Chip, Button, Col,Icon } from 'react-materialize';

class FoodBox extends Component{
    constructor(props){
        super(props)
        this.state={
            qty:1
        }
    }
  
   onChange = e =>{
     const {target} = e;
     const {value} = target;
     this.setState({
         qty: value
     })
     
   }

   onSubmit = e => {
       e.preventDefault();
       this.props.onClick(this.props.food, this.state.qty)
   }
    render(){
        return (
            <div>
             <Col m={3}>
             <Card onSubmit={this.onSubmit} key={this.props.food.id} horizontal header={<CardTitle image={this.props.food.image}></CardTitle>}>
              <p>{this.props.food.name}</p> <Chip>{this.props.food.price}$$$</Chip>
              <input onChange={this.onChange} placeholder="cantidad" type='number' value={this.state.qty}/>
              <Button
              icon='add'
              onClick={this.onSubmit} 
              waves="light" 
              type='submit' 
              >send
              <Icon>add</Icon>    
              </Button>
            </Card>
             </Col>
            </div>
          )
    }
 
}

export default FoodBox
